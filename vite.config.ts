import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import { VitePWA } from 'vite-plugin-pwa';

export default defineConfig({
  server: {
    port: 3013,
  },
  build: {
    minify: true,
    manifest: true,
    cssCodeSplit: false,
  },
  plugins: [
    react(),
    VitePWA({
      registerType: 'prompt',
      minify: true,
      injectRegister: 'auto',
      workbox: {
        globPatterns: ['**/*.{js,css,html,ico,png,svg,ts,tsx,jsx}'],
      },
      includeAssets: ['vite.svg', 'apple-touch-icon.png', 'masked-icon.svg'],
      manifest: {
        name: 'Money2Go',
        short_name: 'm2g',
        description: 'My Awesome App description',
        theme_color: '#ffffff',
        icons: [
          {
            src: '/vite-logo.png',
            sizes: '192x192',
            type: 'image/png',
          },
          {
            src: '/vite-logo.png',
            sizes: '512x512',
            type: 'image/png',
          },
        ],
      },
    }),
  ],
});
